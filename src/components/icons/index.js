import iconAutomobile from './iconAutomobile'
import iconBarChart from './iconBarChart'
import iconBolt from './iconBolt'
import iconBook from './iconBook'
import iconCalendar from './iconCalendar'
import iconUmbrella from './iconUmbrella'
import iconUser from './iconUser'
import iconMenu from './iconMenu'
import iconHeader from './iconHeader'

export {
  iconAutomobile,
  iconBarChart,
  iconBolt,
  iconBook,
  iconCalendar,
  iconMenu,
  iconUmbrella,
  iconUser,
  iconHeader
}
