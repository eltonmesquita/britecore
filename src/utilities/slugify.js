/**
 * Slugifies a string. It returns a slug based on the
 * string recieved. Not safe for non-english characters.
 * Author: Andrew Stewart
 * Source: https://andrew.stwrt.ca/posts/js-slugify/
 *
 * @param {string} string - The string to be slugified
 */

export default function slugify(string) {
  return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w-]+/g, '')
    .replace(/--+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '')
}
