# BriteCore

> UI Engineer Design Team Project

## About it
- Built with Vue.js + SCSS
- No external library besides Vue.js and a small function to slugify strings
- All the (S)CSS was built from the ground up for the project, using only a small [boilerplate](https://github.com/eltonmesquita/polygon) created by myself
- It's totally responsive, make sure to have a look on small screens ;D
- I used the Vue-cli to kick start the project
- I've used [Git Time Metrics](https://github.com/git-time-metric/gtm), so you can see how much time I've spent on the project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
